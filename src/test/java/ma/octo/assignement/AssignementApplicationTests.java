package ma.octo.assignement;

import ma.octo.assignement.web.CompteController;
import ma.octo.assignement.web.UtilisateurController;
import ma.octo.assignement.web.VersementController;
import ma.octo.assignement.web.VirementController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class AssignementApplicationTests {

    @Autowired
    private CompteController compteController;

    @Autowired
    private UtilisateurController utilisateurController;

    @Autowired
    private VersementController versementController;

    @Autowired
    private VirementController virementController;

    @Test
    void contextLoads() {
        assertThat(compteController).isNotNull();
        assertThat(utilisateurController).isNotNull();
        assertThat(versementController).isNotNull();
        assertThat(virementController).isNotNull();
    }

}
