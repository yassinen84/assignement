package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
public class VersementServiceTest {
    private final VersementDto versementDto = new VersementDto();
    private final Compte compteBeneficiaire = new Compte();
    @Autowired
    private VersementService versementService;
    @MockBean
    private CompteRepository compteRepository;
    @MockBean
    private VersementRepository versementRepository;
    @MockBean
    private AuditService auditService;

    @BeforeEach
    void init() {

        // Setting up user
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setUsername("user1");
        utilisateur.setLastname("last1");
        utilisateur.setFirstname("first1");
        utilisateur.setGender(Gender.MALE);

        // setting valid account
        compteBeneficiaire.setNrCompte("010000B025001001");
        compteBeneficiaire.setRib("RIB3");
        compteBeneficiaire.setSolde(BigDecimal.valueOf(140000L));
        compteBeneficiaire.setUtilisateur(utilisateur);

        // Setting a valid virementDto
        versementDto.setMontantVersement(BigDecimal.valueOf(10));
        versementDto.setMotif("monMotif");
        versementDto.setDate(new Date());
        versementDto.setNomCompletEmetteur("Yassine Naciri");
        versementDto.setNrCompteBeneficiaire(compteBeneficiaire.getNrCompte());

        // Mocking methods
        Mockito.when(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).thenReturn(compteBeneficiaire);
        Mockito.when(versementRepository.save(any(Versement.class))).thenReturn(new Versement());
        Mockito.doNothing().when(auditService).auditVersement(any(VersementDto.class));
    }

    @Test
    void shoudlAdd10ToSolde() throws TransactionException, CompteNonExistantException {
        versementDto.setMontantVersement(BigDecimal.valueOf(10));
        int initialSoldeBeneficiaire = compteBeneficiaire.getSolde().intValue();
        versementService.executeVersement(versementDto);
        assertEquals(compteBeneficiaire.getSolde().intValue(), initialSoldeBeneficiaire + 10);
    }

}
