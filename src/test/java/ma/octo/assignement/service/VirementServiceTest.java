package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
public class VirementServiceTest {
    private final VirementDto virementDto = new VirementDto();
    private final Compte compteBeneficiaire = new Compte();
    private final Compte compteEmetteur = new Compte();

    @Autowired
    private VirementService virementService;
    @MockBean
    private CompteRepository compteRepository;
    @MockBean
    private VirementRepository virementRepository;
    @MockBean
    private AuditService auditService;

    @BeforeEach
    void init() {
        // Setting up users
        Utilisateur utilisateur1 = new Utilisateur();
        utilisateur1.setUsername("Naciriyassine");
        utilisateur1.setLastname("Naciri");
        utilisateur1.setFirstname("yassine");
        utilisateur1.setGender(Gender.MALE);

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("BougarneAhmed");
        utilisateur2.setLastname("Bougarne");
        utilisateur2.setFirstname("Ahmed");
        utilisateur2.setGender(Gender.MALE);

        // setting valid accounts
        compteEmetteur.setNrCompte("010000A000001000");
        compteEmetteur.setRib("RIB1");
        compteEmetteur.setSolde(BigDecimal.valueOf(200000L));
        compteEmetteur.setUtilisateur(utilisateur1);

        compteBeneficiaire.setNrCompte("010000B025001000");
        compteBeneficiaire.setRib("RIB2");
        compteBeneficiaire.setSolde(BigDecimal.valueOf(140000L));
        compteBeneficiaire.setUtilisateur(utilisateur2);
        // Setting a valid virementDto
        virementDto.setMontantVirement(BigDecimal.valueOf(10));
        virementDto.setMotif("monMotif");
        virementDto.setDate(new Date());
        virementDto.setNrCompteEmetteur(compteEmetteur.getNrCompte());
        virementDto.setNrCompteBeneficiaire(compteBeneficiaire.getNrCompte());
        // Mocking needed external methods
        Mockito.when(compteRepository.findByNrCompte(compteEmetteur.getNrCompte())).thenReturn(compteEmetteur);
        Mockito.when(compteRepository.findByNrCompte(compteBeneficiaire.getNrCompte())).thenReturn(compteBeneficiaire);
        Mockito.when(virementRepository.save(any(Virement.class))).thenReturn(new Virement());
        Mockito.doNothing().when(auditService).auditVirement(any(VirementDto.class));
    }

    @Test
    void shoudlAdd10ToSolde()
            throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        virementDto.setMontantVirement(BigDecimal.valueOf(10));
        int initialSoldeEmetteur = compteEmetteur.getSolde().intValue();
        int initialSoldeBeneficiaire = compteBeneficiaire.getSolde().intValue();
        virementService.executeVirement(virementDto);
        assertEquals( initialSoldeEmetteur - 10,compteEmetteur.getSolde().intValue());
        assertEquals( initialSoldeBeneficiaire + 10,compteBeneficiaire.getSolde().intValue());
    }


}
