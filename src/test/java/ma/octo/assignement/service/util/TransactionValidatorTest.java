package ma.octo.assignement.service.util;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class TransactionValidatorTest {
    @Autowired
    private TransactionValidator transactionValidator;
    private final Compte compteBeneficiaire = new Compte();
    private final Compte compteEmetteur = new Compte();

    @BeforeEach
    void init(){

        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setUsername("yassinenaciri");
        utilisateur.setLastname("naciri");
        utilisateur.setFirstname("yassine");
        utilisateur.setGender(Gender.MALE);

        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("BougarneAhmed");
        utilisateur2.setLastname("Bougarne");
        utilisateur2.setFirstname("Ahmed");
        utilisateur2.setGender(Gender.MALE);

        compteEmetteur.setNrCompte("010000A000001000");
        compteEmetteur.setRib("RIB1");
        compteEmetteur.setSolde(BigDecimal.valueOf(200000L));
        compteEmetteur.setUtilisateur(utilisateur2);

        compteBeneficiaire.setNrCompte("010000B025001000");
        compteBeneficiaire.setRib("RIB2");
        compteBeneficiaire.setSolde(BigDecimal.valueOf(140000L));
        compteBeneficiaire.setUtilisateur(utilisateur);
    }

    @Test
    void shouldNotThrowExceptionForValidVersement() throws TransactionException, CompteNonExistantException {
        transactionValidator.validateVersement(compteBeneficiaire,BigDecimal.valueOf(9),"mon motif" , "yassine");
    }

    @Test
    void shouldNotThrowExceptionForValidVirement() throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        transactionValidator.validateVirement(compteEmetteur,compteBeneficiaire,BigDecimal.valueOf(100),"mon motif");
    }


    @Test
    void shouldThrowCompteNonExistantException()  throws TransactionException, CompteNonExistantException {
        assertThrows(CompteNonExistantException.class, () -> transactionValidator.validateVersement(null, BigDecimal.valueOf(100),
            "mon motif", "yassine"));
    }

    @Test
    void shouldThrowTransactionExceptionForInvalidCompteBeneficiaire(){
        assertThrows(TransactionException.class, () -> transactionValidator.validateVersement(compteBeneficiaire, BigDecimal.valueOf(0),
            "mon motif", "yassine"));
        assertThrows(TransactionException.class, () -> transactionValidator.validateVersement(compteBeneficiaire, null,
            "mon motif", "yassine"));
    }
}
