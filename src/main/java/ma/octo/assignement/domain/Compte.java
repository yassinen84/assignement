package ma.octo.assignement.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "COMPTE")
public class Compte {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 16, unique = true, nullable = false)
    private String nrCompte;

    @Column(length = 16, unique = true, nullable = false)
    private String rib;

    @Column(precision = 16, scale = 2, nullable = false)
    private BigDecimal solde;

    @ManyToOne()
    @JoinColumn(name = "utilisateur_id", nullable = false)
    private Utilisateur utilisateur;

    public String getNrCompte() {
        return nrCompte;
    }

    public void setNrCompte(String nrCompte) {
        this.nrCompte = nrCompte;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public Utilisateur getUtilisateur() {
        return utilisateur;
    }

    // Making setSolde synchronized to avoid two threads subtracting
    public synchronized void setSolde(BigDecimal solde) {
        this.solde = solde;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
