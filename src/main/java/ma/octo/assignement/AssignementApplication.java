package ma.octo.assignement;

import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.VersementService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AssignementApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(AssignementApplication.class, args);
    }

    @Override
    public void run(String... strings) {

    }
}
