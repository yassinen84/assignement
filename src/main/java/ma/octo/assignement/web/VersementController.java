package ma.octo.assignement.web;


import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VersementService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/versements")
public class VersementController {
    private final VersementService versementService;

    public VersementController(VersementService versementService) {
        this.versementService = versementService;
    }

    @GetMapping
    List<Versement> loadAll() {
        return versementService.getAll();
    }


    @PostMapping("/executer")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VersementDto versementDto)
        throws CompteNonExistantException, TransactionException {
        versementService.executeVersement(versementDto);
    }
}
