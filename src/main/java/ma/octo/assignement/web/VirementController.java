package ma.octo.assignement.web;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VirementService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


//Added path for controller
@RestController
@RequestMapping("/virements")
public class VirementController {

    private final VirementService virementService;

    public VirementController(VirementService virementService) {
        this.virementService = virementService;
    }

    @GetMapping
    List<Virement> loadAll() {
        return virementService.getAll();
    }



    //Virement execution responsibility assigned to VirementService class
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VirementDto virementDto)
        throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        virementService.executeVirement(virementDto);
    }

}
