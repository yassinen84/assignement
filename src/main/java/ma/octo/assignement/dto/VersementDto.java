package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

public class VersementDto {
    private String nomCompletEmetteur;
    private String nrCompteBeneficiaire;
    private String motif;
    private BigDecimal montantVersement;
    private Date date;

    public String getNomCompletEmetteur() {
        return nomCompletEmetteur;
    }

    public void setNomCompletEmetteur(String nomCompletEmetteur) {
        this.nomCompletEmetteur = nomCompletEmetteur;
    }

    public String getNrCompteBeneficiaire() {
        return nrCompteBeneficiaire;
    }

    public void setNrCompteBeneficiaire(String nrCompteBeneficiaire) {
        this.nrCompteBeneficiaire = nrCompteBeneficiaire;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public BigDecimal getMontantVersement() {
        return montantVersement;
    }

    public void setMontantVersement(BigDecimal montantVersement) {
        this.montantVersement = montantVersement;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
