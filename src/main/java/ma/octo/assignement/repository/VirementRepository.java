package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

public interface VirementRepository extends JpaRepository<Virement, Long> {

    int countAllByDateExecutionBetweenAndCompteEmetteur(Date dateDebut,Date dateFin , Compte compteEmeteur);
}
