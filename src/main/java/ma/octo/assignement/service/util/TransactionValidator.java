package ma.octo.assignement.service.util;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VirementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

//class responsible for validating Versement and Virement
@Service
public class TransactionValidator {
    private static final int MONTANT_MAXIMAL = 10000;

    @Autowired
    private VirementRepository virementRepository;

    public void validateVersement(Compte comptebeneficiaire, BigDecimal montant, String motif, String nomEmetteur)
            throws CompteNonExistantException, TransactionException {
        validateCompte(comptebeneficiaire);
        validateMontant(montant);
        validateMotif(motif);
        validateNomComplet(nomEmetteur);
    }

    public void validateVirement(Compte compteEmeteur, Compte compteBeneficiaire, BigDecimal montant, String motif)
        throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        validateCompte(compteEmeteur);
        validateCompte(compteBeneficiaire);
        validateMontant(montant);
        validateSolde(compteEmeteur.getSolde(), montant);
        validateMotif(motif);
    }

    private void validateCompte(Compte compteEmeteur) throws CompteNonExistantException {
        if (compteEmeteur == null) {
            throw new CompteNonExistantException("Compte Non existant");
        }
    }

    private void validateMontant(BigDecimal montant) throws TransactionException {
        // Using '==' instead of 'equals()'
        if (montant == null) {
            throw new TransactionException("Montant vide");
        } else if (montant.intValue() == 0) {
            throw new TransactionException("Montant vide");
        } else if (montant.intValue() < 10) {
            throw new TransactionException("Montant minimal de virement non atteint");
        } else if (montant.intValue() > MONTANT_MAXIMAL) {
            throw new TransactionException("Montant maximal de virement dépassé");
        }
    }

    private void validateMotif(String motif) throws TransactionException {
        if (motif == null || motif.length() == 0) {
            throw new TransactionException("Motif vide");
        }
    }

    private void validateNomComplet(String nom) throws TransactionException {
        if (nom == null || nom.length() == 0) {
            throw new TransactionException("nom de l'emetteur vide");
        }
    }

    private void validateSolde(BigDecimal solde, BigDecimal montant) throws SoldeDisponibleInsuffisantException {
        if (solde.intValue() - montant.intValue() < 0) {
            // Throwing exception instead of Logging
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }

    }

}
