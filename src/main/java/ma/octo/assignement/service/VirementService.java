package ma.octo.assignement.service;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.util.TransactionValidator;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class VirementService {

    private final CompteRepository compteRepository;
    private final VirementRepository virementRepository;
    private final AuditService auditService;

    private final TransactionValidator transactionValidator;

    public VirementService(CompteRepository compteRepository, VirementRepository virementRepository, AuditService auditService, TransactionValidator transactionValidator) {
        this.compteRepository = compteRepository;
        this.virementRepository = virementRepository;
        this.auditService = auditService;
        this.transactionValidator = transactionValidator;
    }

    public List<Virement> getAll() {
        List<Virement> all = virementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    public void executeVirement(VirementDto virementDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {



        Compte compteEmetteur = compteRepository.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte comptebeneficiaire = compteRepository.findByNrCompte(virementDto.getNrCompteBeneficiaire());

        BigDecimal montant = virementDto.getMontantVirement();

        //Validating accounts, montant and motif first
        transactionValidator.validateVirement(compteEmetteur, comptebeneficiaire, montant, virementDto.getMotif());

        //Updating both solde property of both users
        updateSolde(compteEmetteur, comptebeneficiaire, montant);

        //Creating virement entity
        createVirement(virementDto.getDate(), comptebeneficiaire, compteEmetteur, montant, virementDto.getMotif());

        //Creating AuditVirement entity
        auditService.auditVirement(virementDto);
    }

    private void updateSolde(Compte compteEmetteur, Compte comptebeneficiaire, BigDecimal montant) {
        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(montant));
        compteRepository.save(compteEmetteur);

        comptebeneficiaire.setSolde(new BigDecimal(comptebeneficiaire.getSolde().intValue() + montant.intValue()));
        compteRepository.save(comptebeneficiaire);
    }

    private void createVirement(Date date, Compte comptebeneficiaire, Compte compteEmetteur, BigDecimal montant, String motif) {
        Virement virement = new Virement();
        virement.setDateExecution(date);
        virement.setCompteBeneficiaire(comptebeneficiaire);
        virement.setCompteEmetteur(compteEmetteur);
        virement.setMontantVirement(montant);
        virement.setMotifVirement(motif);

        virementRepository.save(virement);
    }


}
