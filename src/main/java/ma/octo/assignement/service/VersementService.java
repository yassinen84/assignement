package ma.octo.assignement.service;


import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.util.TransactionValidator;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class VersementService {
    private final VersementRepository versementRepository;

    private final CompteRepository compteRepository;

    private final AuditService auditService;

    private final TransactionValidator transactionValidator;

    public VersementService(VersementRepository versementRepository, CompteRepository compteRepository, AuditService auditService, TransactionValidator transactionValidator) {
        this.versementRepository = versementRepository;
        this.compteRepository = compteRepository;
        this.auditService = auditService;
        this.transactionValidator = transactionValidator;
    }

    public List<Versement> getAll() {
        List<Versement> all = versementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return all;
        }
    }

    public void executeVersement(VersementDto versementDto) throws CompteNonExistantException, TransactionException {
        String nomEmetteur = versementDto.getNomCompletEmetteur();
        Compte comptebeneficiaire = compteRepository.findByNrCompte(versementDto.getNrCompteBeneficiaire());

        BigDecimal montant = versementDto.getMontantVersement();

        transactionValidator.validateVersement(comptebeneficiaire, montant, versementDto.getMotif(), versementDto.getNomCompletEmetteur());

        updateSolde(comptebeneficiaire, montant);

        createVersement(versementDto.getDate(), comptebeneficiaire, nomEmetteur, montant, versementDto.getMotif());

        auditService.auditVersement(versementDto);
    }

    private void createVersement(Date date, Compte comptebeneficiaire, String nomEmetteur, BigDecimal montant, String motif) {
        Versement versement = new Versement();
        versement.setMontantVersement(montant);
        versement.setCompteBeneficiaire(comptebeneficiaire);
        versement.setMotifVersement(motif);
        versement.setDateExecution(date);
        versement.setNomCompletEmeteur(nomEmetteur);
        versementRepository.save(versement);
    }

    private void updateSolde(Compte comptebeneficiaire, BigDecimal montant) {
        comptebeneficiaire.setSolde(comptebeneficiaire.getSolde().add(montant));
        compteRepository.save(comptebeneficiaire);
    }

}
