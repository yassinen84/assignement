package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditVirement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.repository.AuditVirementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;


@Service
@Transactional
public class AuditService {

    private final Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    private final AuditVirementRepository auditVirementRepository;

    public AuditService(AuditVirementRepository auditVirementRepository) {
        this.auditVirementRepository = auditVirementRepository;
    }

    public void auditVirement(VirementDto virementDto) {
        String message = createMessageVirement(virementDto);
        LOGGER.info("Audit du virement");

        AuditVirement audit = new AuditVirement();
        audit.setMessage(message);
        auditVirementRepository.save(audit);
    }

    public void auditVersement(VersementDto versementDto) {
        String message = createMessageVersement(versementDto);
        LOGGER.info("Audit du versement");
        AuditVirement audit = new AuditVirement();
        audit.setMessage(message);
        auditVirementRepository.save(audit);
    }

    private String createMessageVirement(VirementDto virementDto) {
        return "Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
            .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
            .toString();
    }

    private String createMessageVersement(VersementDto versementDto) {
        return "Versement de " + versementDto.getNomCompletEmetteur() + " vers " + versementDto
            .getNrCompteBeneficiaire() + " d'un montant de " + versementDto.getMontantVersement()
            .toString();
    }


}
